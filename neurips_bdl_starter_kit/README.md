# NeurIPS BDL Approximate Inference Competition - Starting Kit

This is a starting kit for the BDL competition.

Included are two IPython notebooks (one for PyTorch, one for JAX) that show
how to create an initial submission. Also included is an example submission for reference.

## Colabs
Here are colab links to the notebooks:

* [Getting started [JAX]](https://colab.research.google.com/github/izmailovpavel/neurips_bdl_starter_kit/blob/main/getting_started_jax.ipynb)
* [Getting started [PyTorch]](https://colab.research.google.com/github/izmailovpavel/neurips_bdl_starter_kit/blob/main/getting_started_pytorch.ipynb)
