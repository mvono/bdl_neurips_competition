# README #

### PREPROCESSING FOLDER ###

* ./initialization : research code on initialization for approximate samplers
* ./dim_reduction : research code on dimensionality reduction before launching approximate samplers
* ./mcmc_on_subspace : research code on approximate samplers using dimensionality reduction as a preprocessing step