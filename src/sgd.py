import copy
import sys
import torch
from torch.optim import SGD

sys.path.append("../utils/pytorch")
from losses import log_posterior_fn
from metrics import evaluate_fn


def sgd_model(train_loader, test_loader, net_fn, params_model, num_epochs = 10, momentum_decay = 0.9, lr = 0.001,
              burn_in = 1, num_samples = 1, cylcic = F=1):
    thinning = int((num_epochs - burn_in + 1) / num_samples)
    optimizer = SGD(net_fn.parameters(), lr=lr, momentum=momentum_decay)
    if cyclic:
        scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, T)
    epoch_steps = len(train_loader)
    model_state_dict = copy.deepcopy(net_fn.state_dict())
    test_acc, all_test_probs = evaluate_fn(net_fn, test_loader, model_state_dict)
    res = torch.tensor((num_samples,) + all_test_probs.shape)
    for epoch in range(num_epochs):
        running_loss = 0.0
        total_loss = 0.0
        for i, data in enumerate(train_loader):
            optimizer.zero_grad()
            model_state_dict = copy.deepcopy(net_fn.state_dict())
            loss = - log_posterior_fn(net_fn, model_state_dict, data, **params_model)
            loss.backward()
            optimizer.step()
            if cyclic:
                scheduler.step()
            running_loss += loss.item()
            total_loss += loss.item()
            if i % 100 == 99:  # print every 100 mini-batches
                print('[%d, %5d] loss: %.3f' % (epoch + 1, i + 1, running_loss / 100))
                running_loss = 0.0

        model_state_dict = copy.deepcopy(net_fn.state_dict())
        test_acc, all_test_probs = evaluate_fn(net_fn, test_loader, model_state_dict)
        if (epoch - burn_in) % thinning == 0:
            res[(epoch - burn_in) // thinning] = all_test_probs
        print("Epoch {}".format(epoch))
        print("\tAverage loss: {}".format(total_loss / epoch_steps))
        print("\tTest accuracy: {}".format(test_acc))
    return model_state_dict, all_test_probs
