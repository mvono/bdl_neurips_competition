#!/usr/bin/env python
# coding: utf-8

import numpy as np
import torch
import torch.nn as nn
import copy
import sys

sys.path.append("../utils/pytorch")  # TODO: modify here
from losses import log_posterior_fn
from metrics import evaluate_fn


def init_net_z(net_fn, num_workers = 10, device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")):
    # list of the neural networks on each worker
    net_z = []
    for i in range(num_workers):
        # store the neural network
        net_z.append(copy.deepcopy(net_fn).to(device))  # TODO : verify they are independents
    return net_z


class DGLMC:

    def __init__(self, trainset, testset, net_fn, params_model, num_workers = 10, N = np.ones(10, dtype=int),
                 rho = .02 * torch.ones(10), gamma = .005 * torch.ones(10)):
        # TODO: define in function of the num of workers
        train_loader = DataLoader(trainset, batch_size, shuffle=False)
        test_loader = DataLoader(testset, test_batch_size, shuffle=False)
        #
        self.train_loader = train_loader  # TODO: block le shuffling ? le nombre de worker doit le définir ?
        #
        self.test_loader = test_loader
        # Number of worker to distribute the data
        self.num_workers = num_workers
        # Mini-batch size of each worker
        self.N = N
        # Decide which device we want to run on
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        # Define the parameter rho_i in function of the worker
        self.rho = rho.to(self.device)
        # Define the harmonic average of rho_i
        self.rho_hm = 1 / torch.sum(1 / self.rho).to(self.device)
        # Define the step size
        self.gamma = gamma.to(self.device)
        # Load the saved networks
        self.net_fn = net_fn.to(self.device)
        #
        self.params_model = params_model  # TODO: modify here
        # Define the loss function
        self.criterion = nn.CrossEntropyLoss(reduction='sum')
        # TODO: copy the DNN on each worker
        self.net_z = init_net_z(net_fn, num_workers, self.device)

    def net_z_update(self):  # TODO: modify here!
        self.total_loss = 0.0
        for i, data in enumerate(self.train_loader):
            # the i-th worker do N_i local updates
            for k in range(self.N[i]):
                # zero the parameter gradients
                self.net_z[i].zero_grad()
                #
                model_state_dict = copy.deepcopy(self.net_z[i].state_dict())
                # compute the loss  # TODO: there might be an issue with the loss here
                loss = - log_posterior_fn(self.net_z[i], model_state_dict, data, **self.params_model)
                # compute the gradient of loss with respect to all Tensors with requires_grad=True
                loss.backward()
                # disable gradient calculation to reduce memory consumption
                with torch.no_grad():
                    for name, param in self.net_z[i].named_parameters():
                        # perform the ULA step  # TODO: compare with dict(self.net_fn.named_parameters())
                        param.copy_(
                            param + self.gamma[i] / self.rho[i] * (self.net_fn.state_dict()[name] - param) - self.gamma[
                                i] * param.grad.data + torch.sqrt(2 * self.gamma[i]) * torch.randn(param.shape).to(
                                self.device))
            #
            self.total_loss += loss.item()

    def net_fn_update(self):  # TODO: modify this !
        # Parameter updates
        with torch.no_grad():
            for name, param_fn in self.net_fn.named_parameters():
                mu = torch.zeros_like(param_fn)
                for i, net in enumerate(self.net_z):
                    param_z = dict(net.named_parameters())[name]
                    mu += param_z / self.rho[i]
                self.net_fn.copy_(
                    self.rho_hm * mu + torch.sqrt(self.rho_hm) * torch.randn(param_fn.shape).to(self.device))

    def save_results(self, epoch, burn_in, thinning):
        self.model_state_dict = copy.deepcopy(self.net_fn.state_dict())
        test_acc, self.all_test_probs = evaluate_fn(self.net_fn, self.test_loader, self.model_state_dict)
        if (epoch - burn_in) % thinning == 0:
            self.res[(epoch - burn_in) // thinning] = self.all_test_probs
        print("Epoch {}".format(epoch))
        print("\tAverage loss: {}".format(self.total_loss / self.num_workers))  # TODO: modify here
        print("\tTest accuracy: {}".format(test_acc))

    def run(self, num_epochs = 10, num_samples = 10, burn_in = 0):
        #
        thinning = int((num_epochs - burn_in + 1) / num_samples)
        #  # TODO: the output is maybe weird
        for epoch in range(num_epochs):
            self.net_z_update()
            self.net_fn_update()
            self.save_results(epoch, burn_in, thinning)
        return self.model_state_dict, self.all_test_probs


if __name__ == '__main__':  # TODO: an example of a test to explain how the algorithm works
    import matplotlib.pyplot as plt
    import scipy.stats as ss


    class Gaussian:

        def __init__(self, mu, cov):
            self.mu, self.cov = mu, cov
            self.cov_inv = np.linalg.inv(cov)

        def __call__(self, theta):
            return ss.multivariate_normal.pdf(theta, mean=self.mu, cov=self.cov)

        def minus_grad_log(self, theta):
            return np.dot(self.cov_inv, theta - self.mu)


    #
    def net_fn():
        pass  # TODO: we want a neural network here


    # fix the seed for reproducibility
    np.random.seed(20)
    # define the dimension
    dim = 1
    # initialize the parameters
    theta0 = np.zeros(dim)
    # set the number of workers
    num_workers = 10
    # define the number of iterations executed by each worker
    N = 10 * np.random.randint(1, 2, num_workers)
    # define the tolerance parameter
    rho = .01 * np.ones(num_workers)
    # define the stepsize parameter
    gamma = .001 * rho
    # TODO: modify here
    for i in range(num_workers):
        mu, cov = np.zeros(dim), np.identity(dim) * num_workers
        gauss = Gaussian(mu, cov)
        grad_U.append(gauss.minus_grad_log)
        gamma[i] = 1 / np.linalg.eigvalsh(cov)[-1]
    # TODO: here
    trainset = None
    testset = None
    #
    params_model = {'prior_variance': 1 / 5}
    # define the DGLMC sampler
    model = DGLMC(trainset, testset, net_fn, params_model, num_workers, N, rho, gamma)
    #
    model.run(num_epochs=100, num_samples=100, burn_in=0)
    # TODO: we want some samples
    weights_dglmc = None
    # display the results
    plt.hist(np.squeeze(weights_dglmc), 10, density=True)
    X = np.linspace(-3, 3, 500)
    plt.plot(X, [ss.multivariate_normal.pdf(x, mean=mu, cov=np.identity(dim)) for x in X])
    plt.grid('True')
    plt.title('N={0:.0}, rho={1:.1E}, gamma={2:.1E}'.format(N[0], rho[0], gamma[0]))
    plt.show()
