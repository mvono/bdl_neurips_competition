import math
import numpy as np
import copy

import torch 
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader, TensorDataset
import torch.optim as optim
import pytorch_models as p_models



def log_likelihood_fn(net_fn, model_state_dict, batch):
    """Computes the log-likelihood."""
    x, y = batch
    if torch.cuda.is_available():
      x = x.cuda()
      y = y.cuda()
    net_fn.zero_grad()
    for name, param in net_fn.named_parameters():
        param.data = model_state_dict[name]
    logits = net_fn(x)
    num_classes = logits.shape[-1]
    labels = F.one_hot(y.to(torch.int64), num_classes= num_classes)
    softmax_xent = torch.sum(labels * F.log_softmax(logits))

    return softmax_xent


def log_prior_fn(model_state_dict, prior_variance):
    """Computes the Gaussian prior log-density."""
    n_params = sum(p.numel() for p in model_state_dict.values()) 
    exp_term = sum((-p**2 / (2 * prior_variance)).sum() for p in model_state_dict.values() )
    norm_constant = -0.5 * n_params * math.log((2 * math.pi * prior_variance))
    return exp_term + norm_constant


def log_posterior_fn(net_fn, model_state_dict, batch, prior_variance=1.):
    log_lik = log_likelihood_fn(net_fn, model_state_dict, batch)
    log_prior = log_prior_fn(model_state_dict, prior_variance)
    return log_lik + log_prior

