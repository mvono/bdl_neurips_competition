import numpy as np
import copy
import os
import torch
import neurips_bdl_starter_kit.pytorch_models as p_models
from torch.utils.data import DataLoader, TensorDataset
from utils.pytorch.metrics import agreement, total_variation_distance
from src import sgd

files = os.listdir()
if 'cifar10_train_x.csv' not in files:
    print('Downloading the files... This might be long')
    os.system('gsutil -m cp -r gs://neurips2021_bdl_competition/cifar10_*.csv .')

''' 
Find a way to import dataset more efficiently !!
'''

print('Loading the files... Just a few minutes !')

x_train = np.loadtxt("cifar10_train_x.csv")
y_train = np.loadtxt("cifar10_train_y.csv")

x_test = np.loadtxt("cifar10_test_x.csv")
y_test = np.loadtxt("cifar10_test_y.csv")

x_train = x_train.reshape((len(x_train), 3, 32, 32))
x_test = x_test.reshape((len(x_test), 3, 32, 32))

trainset = TensorDataset(torch.Tensor(x_train), torch.Tensor(y_train))
testset = TensorDataset(torch.Tensor(x_test), torch.Tensor(y_test))

''' 
Find a way to import dataset more efficiently !!


transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=True, transform=transform)
#trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
#                                          shuffle=True, num_workers=2)

testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                       download=True, transform=transform)
#testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size,
#                                         shuffle=False, num_workers=2)
'''

## To GPU

os.system('nvidia-smi')

net_fn = p_models.get_model("resnet20_frn_swish", data_info={"num_classes": 10})
if torch.cuda.is_available():
    print("GPU available!")
    net_fn = net_fn.cuda()
model_state_dict = copy.deepcopy(net_fn.state_dict())

## Optimization and training

batch_size = 100
test_batch_size = 100

train_loader = DataLoader(trainset, batch_size, shuffle=True)
test_loader = DataLoader(testset, test_batch_size, shuffle=False)

params_model = {'prior_variance': 1 / 5}
params = dict()
params['num_epochs'] = 2
params['momentum_decay'] = 0.9
params['lr'] = 0.01
params['cyclic'] = True
params['T'] = 10

# Define the model
model = sgd.sgd_model

model_state_dict, all_test_probs = model(train_loader, test_loader, net_fn, params_model, **params)
all_test_probs = np.asarray(all_test_probs.cpu())

## Evaluating metrics

# We can load the HMC reference predictions from the starter kit as well.

with open('./data/cifar10/probs.csv', 'r') as fp:
    reference = np.loadtxt(fp)

# Now we can compute the metrics!

agreement(all_test_probs, reference)
total_variation_distance(all_test_probs, reference)

## Preparing the submission

np.savetxt("cifar10_probs.csv", all_test_probs)

os.system('zip submission.zip cifar10_probs.csv')
